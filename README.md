Rodney's Click Server v0.1.2
============================
[![Docker Repository on Quay](https://quay.io/repository/rodney-lai/click/status "Docker Repository on Quay")](https://quay.io/repository/rodney-lai/click)

rlai [ at ] irismedia [ dot ] com

[GitHub Repository](https://github.com/rodney-lai)

[BitBucket Repository](https://bitbucket.org/rodney-lai)

[Quay Docker Repository](https://quay.io/user/rodney-lai)

Server Technology
-----------------

* Web Framework - [DotNet Core 3.1](https://www.microsoft.com/net/core/)
* Language - [C#](https://docs.microsoft.com/en-us/dotnet/csharp/)
* NoSQL Database - [MongoDB](https://www.mongodb.org/)
* SQL Database - [PostgreSQL](https://www.postgresql.org/)

Configuration
-------------

Click urls are of the form `http://localhost:5000/{click_type}/{id}`  
Clicks will be recorded in Mongo or Postgres (if configured),
then redirected to a configured web site.

Environment Variables:

Required:

Click\_\_Urls\_\_WebSite - the web site to redirect to.  
Click\_\_Types - a list of valid click types separated by a pipe, |  

Optional:

ConnectionStrings\_\_MongoDB_LoggingDatabase - mongo connection string  
mongodb://{user}:{password}@{host}/{database}  
ConnectionStrings\_\_Postgres_LoggingDatabase - npgsql postgres connection string  
Server={host};Port={post};Database={database};User Id={user};Password={password};  

Copyright (c) 2017-2020 Rodney S.K. Lai

Permission to use, copy, modify, and/or distribute this software for
any purpose with or without fee is hereby granted, provided that the
above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
