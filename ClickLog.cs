/**
 *
 * Copyright (c) 2017 Rodney S.K. Lai
 * https://github.com/rodney-lai
 * https://bitbucket.org/rodney-lai/
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace com.rodneylai.server.click {

    public partial class ClickLog {
        [BsonIgnoreAttribute]
        public long ClickLogId { get; set; }
        public string ClickType { get; set; }
        public Guid? ClickUuid { get; set; }
        public long? ClickId { get; set; }
        public string UserAgent { get; set; }
        public string IpAddress { get; set; }
        public Guid TrackingUuid { get; set; }
        public DateTime Created { get; set; }
    }
}
