/**
 *
 * Copyright (c) 2017-2020 Rodney S.K. Lai
 * https://github.com/rodney-lai
 * https://bitbucket.org/rodney-lai/
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using HashidsNet;
using MongoDB.Bson;
using MongoDB.Driver;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;

namespace com.rodneylai.server.click {

  public class DefaultRouter : IRouter {

    private static IConfigurationSection m_clickSection = Program.Configuration.GetSection("Click");
    private static IConfigurationSection m_urlsSection = (m_clickSection == null) ? null : m_clickSection.GetSection("Urls");
    private static string m_webSiteUrl = ((m_urlsSection == null) || (m_urlsSection["WebSite"] == null)) ? "http://localhost:5000" : m_urlsSection["WebSite"];

    public Task RouteAsync(RouteContext context) {
      context.Handler = c => {
        c.Response.Redirect(m_webSiteUrl);
        return (Task.FromResult(0));
      };
      return (Task.FromResult(0));
    }

    public VirtualPathData GetVirtualPath(VirtualPathContext context) {
      return (null);
    }
  }

  public class PingRouter : IRouter {

    public Task RouteAsync(RouteContext context) {
      context.Handler = async c => {
        await c.Response.WriteAsync(String.Format("okay[{0}][dotnet_version={1}]",DateTime.Now,Environment.Version));
      };
      return (Task.FromResult(0));
    }

    public VirtualPathData GetVirtualPath(VirtualPathContext context) {
      return (null);
    }
  }

  public class ClickRouter : IRouter
  {
    private readonly ILogger m_log;
    private static IConfigurationSection m_clickSection = Program.Configuration.GetSection("Click");
    private static IConfigurationSection m_s3Section = (m_clickSection == null) ? null : m_clickSection.GetSection("S3");
    private static string m_s3Bucket = ((m_s3Section == null) || (m_s3Section["Bucket"] == null)) ? String.Empty : m_s3Section["Bucket"];
    private static string m_s3Region = ((m_s3Section == null) || (m_s3Section["Region"] == null)) ? String.Empty : m_s3Section["Region"];
    private static IConfigurationSection m_urlsSection = (m_clickSection == null) ? null : m_clickSection.GetSection("Urls");
    private static string m_webSiteUrl = ((m_urlsSection == null) || (m_urlsSection["WebSite"] == null)) ? "http://localhost:5000" : m_urlsSection["WebSite"];
    private static string m_mobileAppUrl = ((m_urlsSection == null) || (m_urlsSection["MobileApp"] == null)) ? m_webSiteUrl : m_urlsSection["MobileApp"];
    private static string m_itunesAppUrl = ((m_urlsSection == null) || (m_urlsSection["iTunes"] == null)) ? m_webSiteUrl : m_urlsSection["iTunes"];
    private static IConfigurationSection m_hashIdsSection = (m_clickSection == null) ? null : m_clickSection.GetSection("HashIds");
    private static string m_hashIdsSecret = ((m_hashIdsSection == null) || (m_hashIdsSection["Secret"] == null)) ? null : m_hashIdsSection["Secret"];
    private static Hashids m_hashids = String.IsNullOrWhiteSpace(m_hashIdsSecret) ? null : new Hashids(m_hashIdsSecret, 10);
    private static string m_mongoConnectionString = Program.Configuration.GetConnectionString("MongoDB_LoggingDatabase");
    private static string m_mongoDatabaseName = String.IsNullOrEmpty(m_mongoConnectionString) ? null : m_mongoConnectionString.Substring(m_mongoConnectionString.LastIndexOf("/") + 1);
    private static MongoClient m_mongoClient = (m_mongoConnectionString == null) ? null : new MongoClient(m_mongoConnectionString);
    private static IMongoDatabase m_mongoDatabase = ((m_mongoClient == null) || (String.IsNullOrEmpty(m_mongoDatabaseName))) ? null : m_mongoClient.GetDatabase(m_mongoDatabaseName);
    private static IMongoCollection<ClickLog> m_mongoClickLogCollection = (m_mongoDatabase == null) ? null : m_mongoDatabase.GetCollection<ClickLog>("ClickLog");
    private Dictionary<String, String> m_template = new Dictionary<String, String>();

    public ClickRouter(ILogger<ClickRouter> log) {
      string clickTypes = ((m_clickSection == null) || (m_clickSection["Types"] == null)) ? null : m_clickSection["Types"];

      m_log = log;
      if (!String.IsNullOrEmpty(clickTypes)) {
        Init(clickTypes.Split('|'));
      }
    }

    private async void Init(string[] clickTypes) {
      foreach (string clickType in clickTypes) {
        string template = await ReadS3File(clickType);

        if (!String.IsNullOrWhiteSpace(template)) m_template.Add(clickType, template);
      }
    }

    private async Task<string> ReadS3File(string clickType) {
      if (String.IsNullOrEmpty(m_s3Bucket)) {
        return (String.Empty);
      } else {
        AmazonS3Client client = new AmazonS3Client(String.IsNullOrEmpty(m_s3Region) ? RegionEndpoint.USEast1 : RegionEndpoint.GetBySystemName(m_s3Region));

        try {
          using (GetObjectResponse response = await client.GetObjectAsync(m_s3Bucket, "click/" + clickType + ".html")) {
            using (StreamReader reader = new StreamReader(response.ResponseStream)) {
              return (reader.ReadToEnd());
            }
          }
        } catch (AmazonS3Exception ex) {
          if (ex.ErrorCode != "NoSuchKey") m_log.LogError(1000, ex, "ReadS3File");
          return (String.Empty);
        }
      }
    }

    private bool IsMobile(String userAgent) {
      Regex b = new Regex(@"(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
      Regex v = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);

      return ((b.IsMatch(userAgent) || v.IsMatch(userAgent.Substring(0, 4))));
    }

    public bool ParseHashId(string id, out long? clickId) {
      clickId = null;
      if (m_hashids == null) {
        return (false);
      } else {
        long[] numbers = m_hashids.DecodeLong(id);

        if (numbers.Length == 0) {
          return (false);
        } else {
          clickId = numbers[0];
          return (true);
        }
      }
    }

    public Task RouteAsync(RouteContext context) {
      context.Handler = async c => {
        string clickType = c.GetRouteValue("click_type").ToString();
        var id = c.GetRouteValue("id");
        Guid clickGuid;
        long? clickId = null;

        if ((Guid.TryParse(id.ToString(), out clickGuid)) || (ParseHashId(id.ToString(), out clickId))) {
          Guid trackingGuid;
          String userAgent = c.Request.Headers["User-Agent"].ToString();
          String ipAddress = c.Request.Headers["X-Forwarded-For"].ToString();

          if (String.IsNullOrEmpty(ipAddress)) {
            ipAddress = c.Features.Get<IHttpConnectionFeature>()?.RemoteIpAddress.ToString();
          }
          if ((c.Request.Cookies["tracking_id"] == null) || (!Guid.TryParse(c.Request.Cookies["tracking_id"].ToString(), out trackingGuid))) {
            CookieOptions options = new CookieOptions();

            trackingGuid = Guid.NewGuid();
            options.Expires = DateTime.Now.AddYears(10);
            c.Response.Cookies.Append("tracking_id", trackingGuid.ToString(), options);
          }
          if (LoggingDbContext.IsActive()) {
            using (var db = new LoggingDbContext()) {
              var clickLog = new ClickLog {
                ClickType = clickType,
                ClickUuid = (clickGuid == Guid.Empty) ? (Guid?)null : clickGuid,
                ClickId = clickId,
                UserAgent = userAgent,
                IpAddress = ipAddress,
                TrackingUuid = trackingGuid
              };
              await db.ClickLog.AddAsync(clickLog);
              await db.SaveChangesAsync();
            }
          }
          if (m_mongoClickLogCollection != null) {
            await m_mongoClickLogCollection.InsertOneAsync(new ClickLog {
              ClickType = clickType,
              ClickUuid = (clickGuid == Guid.Empty) ? (Guid?)null : clickGuid,
              ClickId = clickId,
              UserAgent = userAgent,
              IpAddress = ipAddress,
              TrackingUuid = trackingGuid,
              Created = DateTime.UtcNow
            });
          }
          if (IsMobile(userAgent)) {
            string redirectUrl = m_mobileAppUrl + "/" + clickType + "/" + ((clickGuid == Guid.Empty) ? id.ToString() : clickGuid.ToString("D"));

            if (m_template.ContainsKey(clickType)) {
              await c.Response.WriteAsync(m_template[clickType].Replace("@(redirectUrl)",redirectUrl));
            } else {
              c.Response.Redirect(redirectUrl);
            }
          } else {
            c.Response.Redirect(m_webSiteUrl + "/" + clickType + ".html?id=" + ((clickGuid == Guid.Empty) ? id.ToString() : clickGuid.ToString("D")));
          }
        } else {
          c.Response.Redirect(m_webSiteUrl);
        }
      };
      return (Task.FromResult(0));
    }

    public VirtualPathData GetVirtualPath(VirtualPathContext context) {
      return (null);
    }
  }

  public class Startup {

    public Startup() {
      MongoDefaults.GuidRepresentation = MongoDB.Bson.GuidRepresentation.Standard;
    }

    public void ConfigureServices(IServiceCollection services) {
      services.AddRouting();
      services.AddLogging(loggingBuilder =>
      {
        loggingBuilder.AddConsole();
      });
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory) {
      if (env.IsDevelopment()) {
        app.UseDeveloperExceptionPage();
      }

      RouteBuilder routeBuilder = new RouteBuilder(app);
      IConfigurationSection clickSection = Program.Configuration.GetSection("Click");
      string clickTypes = ((clickSection == null) || (clickSection["Types"] == null)) ? null : clickSection["Types"];

      if (!String.IsNullOrEmpty(clickTypes)) {
        routeBuilder.Routes.Add(new Route(new ClickRouter(loggerFactory.CreateLogger<ClickRouter>()), "{click_type:regex(^" + clickTypes + "$)}/{id}", app.ApplicationServices.GetService<IInlineConstraintResolver>()));
      }
      routeBuilder.Routes.Add(new Route(new PingRouter(), "ping", app.ApplicationServices.GetService<IInlineConstraintResolver>()));
      routeBuilder.Routes.Add(new Route(new DefaultRouter(), "{*url}", app.ApplicationServices.GetService<IInlineConstraintResolver>()));
      app.UseRouter(routeBuilder.Build());

    }
  }

}
