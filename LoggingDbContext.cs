/**
 *
 * Copyright (c) 2017 Rodney S.K. Lai
 * https://github.com/rodney-lai
 * https://bitbucket.org/rodney-lai/
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

using System;
using System.Diagnostics;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace com.rodneylai.server.click {

    public partial class LoggingDbContext : DbContext {

        private static string m_connectionString = Program.Configuration.GetConnectionString("Postgres_LoggingDatabase");

        public static bool IsActive() {
            return(!String.IsNullOrEmpty(m_connectionString));
        }

        public virtual DbSet<ClickLog> ClickLog { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            optionsBuilder.UseNpgsql(m_connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            var section = Program.Configuration.GetSection("Database");
            var schema = section["Schema"];

            if (schema != null) {
                modelBuilder.HasDefaultSchema(schema);
            }
            modelBuilder.Entity<ClickLog>(entity =>
            {
                entity.ToTable("Click_Log");

                entity.Property(e => e.ClickLogId)
                    .HasColumnName("Click_Log_Id")
                    .HasDefaultValueSql("nextval('\"Click_Log_Click_Log_Id_seq1\"'::regclass)");

                entity.Property(e => e.ClickId).HasColumnName("Click_Id");

                entity.Property(e => e.ClickType)
                    .IsRequired()
                    .HasColumnName("Click_Type")
                    .HasColumnType("varchar")
                    .HasMaxLength(20);

                entity.Property(e => e.ClickUuid).HasColumnName("Click_Uuid");

                entity.Property(e => e.Created)
                    .HasColumnType("timestamptz")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.IpAddress)
                    .IsRequired()
                    .HasColumnName("Ip_Address")
                    .HasColumnType("varchar")
                    .HasMaxLength(20);

                entity.Property(e => e.TrackingUuid).HasColumnName("Tracking_Uuid");

                entity.Property(e => e.UserAgent)
                    .IsRequired()
                    .HasColumnName("User_Agent")
                    .HasColumnType("varchar")
                    .HasMaxLength(200);
            });
        }
    }
}
