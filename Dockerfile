FROM        ubuntu:18.04

MAINTAINER  Rodney Lai <rlai@irismedia.com>

ENV         DEBIAN_FRONTEND noninteractive

RUN         apt-get update

RUN         apt-get upgrade -y -q

RUN         apt-get install -y apt-transport-https

RUN         apt-get install -y wget

RUN         wget https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb

RUN         dpkg -i packages-microsoft-prod.deb

RUN         apt-get update

RUN         apt-get install -y dotnet-sdk-3.1

RUN         update-ca-certificates -f

RUN         useradd -ms /bin/bash dotnet-user

USER        dotnet-user

ADD         . /home/dotnet-user/root/build/click

RUN         cp -r /home/dotnet-user/root/build/ /home/dotnet-user

USER        root

RUN         rm -rf /home/dotnet-user/root

USER        dotnet-user

RUN         cd /home/dotnet-user/build/click; dotnet restore; dotnet publish
RUN         mv /home/dotnet-user/build/click/bin/Debug/netcoreapp3.1/publish /home/dotnet-user/deploy-click
RUN         rm -rf /home/dotnet-user/build

RUN         ls -la /home/dotnet-user
RUN         ls -la /home/dotnet-user/deploy-click

WORKDIR     /home/dotnet-user/deploy-click

ENTRYPOINT  ["/usr/bin/dotnet","click.dll"]
EXPOSE      5000
