CREATE TABLE IF NOT EXISTS "Click_Log" (
  "Click_Log_Id" BIGSERIAL,
  "Click_Type" VARCHAR(20) NOT NULL,
  "Click_Uuid" UUID NULL,
  "Click_Id" BIGINT NULL,
  "User_Agent" VARCHAR(200) NOT NULL,
  "Ip_Address" VARCHAR(20) NOT NULL,
  "Tracking_Uuid" UUID NOT NULL,
  "Created" TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  CONSTRAINT "PK_Click_Log" PRIMARY KEY ("Click_Log_Id")
);
